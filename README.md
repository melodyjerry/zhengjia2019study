# 介绍
- 跟着19级师弟妹一起学习。
> 含学习报告、案例等的远程仓库。

# Demo演示
## 泽伟
待展示……

## 家乐
1. [新闻案例](http://melodyjerry.gitee.io/zhengjia2019study/一些demo/前端/小案例/新闻案例/index.html)
2. [花店展示页面](http://melodyjerry.gitee.io/zhengjia2019study/一些demo/前端/花店展示页面/index.html)
3. [jQ淡出淡入效果](http://melodyjerry.gitee.io/zhengjia2019study/一些demo/前端/小案例/淡入淡出demo.html)


## MelodyJerry
1. [拟态式极简导航](http://melodyjerry.gitee.io/zhengjia2019study/)
2. [纯CSS实现3D翻转卡片](http://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/CSS/3D%E7%BF%BB%E8%BD%AC%E5%8D%A1%E7%89%87/index.html)
3. [CSS3电影播放倒计时读秒代码](http://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/CSS/CSS3%E7%94%B5%E5%BD%B1%E6%92%AD%E6%94%BE%E5%80%92%E8%AE%A1%E6%97%B6%E8%AF%BB%E7%A7%92%E4%BB%A3%E7%A0%81/index.html)
4. [星益小游戏平台](http://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/%E6%B8%B8%E6%88%8F/%E6%98%9F%E7%9B%8A%E5%B0%8F%E6%B8%B8%E6%88%8F%E5%B9%B3%E5%8F%B0/index.html)（搜集自互联网，因版权问题，仅供学习、娱乐使用。）
5. [JS自定义倒计时代码](http://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/JS/JS%E8%87%AA%E5%AE%9A%E4%B9%89%E5%80%92%E8%AE%A1%E6%97%B6%E4%BB%A3%E7%A0%81/index.html)
6. [模仿B站首页Banner背景跟随鼠标虚化](https://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/%E5%B0%8F%E6%A1%88%E4%BE%8B/B%E7%AB%99%E9%A6%96%E9%A1%B5Banner%E8%83%8C%E6%99%AF%E8%B7%9F%E9%9A%8F%E9%BC%A0%E6%A0%87%E8%99%9A%E5%8C%96/index.html)
7. [Happy New Year 2021](https://melodyjerry.gitee.io/zhengjia2019study/%E4%B8%80%E4%BA%9Bdemo/%E5%89%8D%E7%AB%AF/%E5%B0%8F%E6%A1%88%E4%BE%8B/HappyNewYear2021/index.html)